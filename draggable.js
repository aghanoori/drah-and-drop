
class draggable {
    dragSrcElm;
    list;
    update;
    constructor(options){
        this.setupList(options);
        this.list = options.list;
        if(options.update) this.update = options.update;

        for(let listitem of options.el.children) {
            this.addDnDHandler(listitem);
        }
    }

    setupList(options) {
        let {el:element ,list , template} = options;
        
        //etebar sanji(hameye ashya bayad montabegh bashand)
        if(!element) throw Error("element is not exist");
        if(!list) throw Error("the list is not exist");
        if(!Array.isArray(list)) throw Error ("please insert an array");
        if(!template) throw Error("template is not exist");
        if(typeof template!== "function" ) throw Error("please insert a function");

        list.forEach(item => element.innerHTML += template(item));

    };
    /**
     *  drag and drop handler
     * @param Object element
     * @return void 
     */
    addDnDHandler(element){
        element.setAttribute("draggable","true");

        element.addEventListener("dragstart",this.handleDragStart.bind(this));
        element.addEventListener("dragover",this.handleDragOver.bind(this));
        element.addEventListener("dragleave",this.handleDragLeave.bind(this));
        element.addEventListener("drop",this.handleDragDrop.bind(this));
        element.addEventListener("dragend",this.handleDragEnd.bind(this));

    };
    /**
     * drag start event
     * @param Object e
     * @return void 
     */
    handleDragStart(e){
        this.dragSrcElm = e.target;
        //set kardan format va data baraye dragstart (dataTransfer.setData(format,data))
        e.dataTransfer.setData("text/html ",e.target.outerHTML);

        e.target.classList.add('dragElem');
    };

    handleDragOver(e){
        if(e.preventDefault) e.preventDefault();
        e.target.classList.add('over');
    };

    handleDragLeave(e){
        e.target.classList.remove('over');
    };

    handleDragDrop(e){
        //mahale drop shodan.
        let target = e.target.closest('.list-item');

        //baraye inke agar elementi roye khodash darag va drop shod hazf nashavad.
        if(this.dragSrcElm !== target){
            //hazf kardan list item ghabli bad az drop(baraye jologiri az copy shodan)
            target.parentNode.removeChild(this.dragSrcElm);

            //elementi ke drag and drop mishavad .(dataTransfer.getData(format))
            let dropHtml = e.dataTransfer.getData('text/html');

            //gharar dadan target ghabl az drophtml
            target.insertAdjacentHTML('beforebegin' , dropHtml);

            //chon listener pak mishavad dobare inja set mishavad ta betavan chandin bar har element ra drag and drop kard
            this.addDnDHandler(target.previousSibling);
        }
        
        e.target.classList.remove('over');
    };
    
    handleDragEnd(e){
        e.target.classList.remove('dragElem');
        // list update shode ke bad az har drag and drop be server ersal mishavad.
        let newlist = [];
        list.querySelectorAll('.list-item').forEach(elm => newlist.push(this.list.find(item => elm.id == item.id)));
        this.update(newlist);
    };


}
