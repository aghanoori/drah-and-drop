//list mojoud ya listi ke az server daryaft kardim.
let data = [
    {id:1 , title:"آیتم شماره یک", text:"لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود."},
    {id:2 , title:"آیتم شماره دو", text:"لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود."},
    {id:3 , title:"آیتم شماره سه", text:"لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود."},
    {id:4 , title:"آیتم شماره چهار", text:"لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود."},
    {id:5 , title:"آیتم شماره بنج", text:"لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود."},
    {id:6 , title:"آیتم شماره شش", text:"لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود."},
    {id:7 , title:"آیتم شماره هفت", text:"لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود."}
]

//az class draggable yek object sakhtim ke daraya metodha va propertyhayist ke dar method sazande class tarif shode.
new draggable({
    el:document.querySelector("#list"),
    list:data,
    template: (item) => {
        return ` <div class="list-item" id="${item.id}">
                    <div class="list-item_head">
                        <span class="head-id">${item.id}</span>
                    </div>
                    <div class="list-item_content">
                        <span class="item-title">${item.title}</span>
                        <p>${item.text}</p>
                    </div>
                </div>` 
    },
    update:(list) => {
        console.log(list);
    }
}) 